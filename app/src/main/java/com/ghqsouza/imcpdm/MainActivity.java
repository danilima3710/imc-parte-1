package com.ghqsouza.imcpdm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    float peso;
    float altura;
    float imc;
    String valor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void Calcular(View view) {
        final EditText p = (EditText) findViewById(R.id.pesotxt);
        final EditText a = (EditText) findViewById(R.id.alturatxt);
        final TextView r = (TextView) findViewById(R.id.resultado);
        altura = Float.parseFloat(a.getText().toString());
        peso = Float.parseFloat(p.getText().toString());
        imc = (peso) / (altura* altura);

        if (imc <18.5){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Abaixo do peso", Toast.LENGTH_LONG);
            toast.show();
        }

        if (imc >= 18.6 && imc<25){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Peso Ideal", Toast.LENGTH_LONG);
            toast.show();
        }

        if (imc >=25 && imc <30){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Levemente acima do peso", Toast.LENGTH_LONG);
            toast.show();
        }

        if (imc >=30 && imc <35){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Obesidade Grau 1", Toast.LENGTH_LONG);
            toast.show();
        }

        if (imc >=35 && imc < 40){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Obesidade Grau 2", Toast.LENGTH_LONG);
            toast.show();
        }

        if (imc >=40) {
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Obesidade Grau 3", Toast.LENGTH_LONG);
            toast.show();
        }

    }

}
